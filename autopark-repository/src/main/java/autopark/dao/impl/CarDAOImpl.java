package autopark.dao.impl;

import autopark.dao.ICarDAO;
import autopark.domain.Car;
import autopark.domain.CarLoadingCapacityEnum;
import autopark.domain.CarTypeEnum;
import autopark.domain.CarVendorEnum;
import autopark.dto.CarDTO;
import autopark.dto.UserDTO;
import autopark.rawconnection.Connections;
import org.springframework.stereotype.Repository;
import org.springframework.util.StringUtils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


@Repository
public class CarDAOImpl extends RootDAOImpl<Car> implements ICarDAO {


    public CarDAOImpl() {
        super("autopark.domain.Car", Car.class);
    }

    public List<Car> getCarsHibernate() {
        return findAllList();
    }

    public List<CarDTO> getCarsJdbc() {
        List<CarDTO> result = null;


        PreparedStatement preparedStatement = null;
        ResultSet rs = null;
        Connection connection = Connections.getJNDIConnection();
        try {
            preparedStatement = connection.prepareStatement("" +
                    "select \n" +
                    "C.id as carId,\n" +
                    "C.vendor as carVendor,\n" +
                    "C.model as carModel,\n" +
                    "C.type as carType,\n" +
                    "C.capacity as carCapacity,\n" +
                    "C.year as carYear,\n" +
                    "C.description as carDescription,\n" +
                    "C.creationDate as carCreationDate,\n" +
                    "U.id as userId,\n" +
                    "U.name as userName,\n" +
                    "U.email as userEmail,\n" +
                    "U.phone as userPhone " +
                    " from ap_car C " +
                    " left join ap_user U on C.user_id=U.id");


            rs = preparedStatement.executeQuery();
            Map<Long,UserDTO> userMap = null;

            if (rs != null) {
                userMap = new HashMap<Long, UserDTO>();
                result = new ArrayList<CarDTO>();
                while (rs.next()) {
                    Long userId = rs.getLong("userId");
                    UserDTO userDTO = userMap.get(userId);
                    if(userDTO == null){
                        userDTO = new UserDTO();
                        userDTO.setId(userId);
                        userDTO.setName(rs.getString("userName"));
                        userDTO.setEmail(rs.getString("userEmail"));
                        userDTO.setPhone(rs.getString("userPhone"));
                        userMap.put(userId,userDTO);
                    }
                    Long carId = rs.getLong("carId");
                    if(carId == null || carId == 0) continue;
                    CarDTO carDTO = new CarDTO();
                    carDTO.setId(carId);
                    carDTO.setType((CarTypeEnum)extractEnum(rs,"carType", CarTypeEnum.class));
                    carDTO.setVendor((CarVendorEnum)extractEnum(rs,"carVendor", CarVendorEnum.class));
                    carDTO.setCapacity((CarLoadingCapacityEnum)extractEnum(rs,"carCapacity", CarLoadingCapacityEnum.class));
                    carDTO.setModel(rs.getString("carModel"));
                    carDTO.setDescription(rs.getString("carDescription"));
                    carDTO.setYear(rs.getInt("carYear"));
                    carDTO.setCreationDate(rs.getTimestamp("carCreationDate"));
                    carDTO.setUser(userDTO);
                    result.add(carDTO);
                }
            }
            return result;
        } catch (SQLException e) {
            logger.error("Failed to execute a query", e);
        } finally {
            closeAfterRaw(connection, preparedStatement, rs);
        }
        return null;
    }

    private static Enum extractEnum(ResultSet rs,String name, Class enumType) throws SQLException{
        String val = rs.getString(name);
        if (StringUtils.isEmpty(val)){
            return null;
        }
        return Enum.valueOf(enumType,val);
    }

}