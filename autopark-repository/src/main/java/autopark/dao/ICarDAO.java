package autopark.dao;


import autopark.domain.Car;
import autopark.dto.CarDTO;

import java.util.List;

public interface ICarDAO extends IRootDAO<Car> {
    List<Car> getCarsHibernate();
    List<CarDTO> getCarsJdbc();
}