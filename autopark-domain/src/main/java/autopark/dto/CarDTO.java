package autopark.dto;

import autopark.domain.CarLoadingCapacityEnum;
import autopark.domain.CarTypeEnum;
import autopark.domain.CarVendorEnum;

import java.util.Date;


public class CarDTO {
    private Long id;
    private CarVendorEnum vendor;
    private String model;
    private CarTypeEnum type;
    private CarLoadingCapacityEnum capacity;
    private Integer year;
    private String description;
    private UserDTO user;
    private Date creationDate;


    public CarVendorEnum getVendor() {
        return vendor;
    }

    public void setVendor(CarVendorEnum vendor) {
        this.vendor = vendor;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public CarTypeEnum getType() {
        return type;
    }

    public void setType(CarTypeEnum type) {
        this.type = type;
    }

    public CarLoadingCapacityEnum getCapacity() {
        return capacity;
    }

    public void setCapacity(CarLoadingCapacityEnum capacity) {
        this.capacity = capacity;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public UserDTO getUser() {
        return user;
    }

    public void setUser(UserDTO user) {
        this.user = user;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }
}
