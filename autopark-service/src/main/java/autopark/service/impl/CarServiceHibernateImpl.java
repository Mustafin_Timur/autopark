package autopark.service.impl;

import autopark.dao.ICarDAO;
import autopark.domain.Car;
import autopark.domain.User;
import autopark.dto.CarDTO;
import autopark.dto.UserDTO;
import autopark.service.ICarService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
@Service("carServiceHibernate")
public class CarServiceHibernateImpl implements ICarService{

    @Autowired
    private ICarDAO carDAO;


    public List<CarDTO> getCars() {
        List<Car> cars = carDAO.getCarsHibernate();
        List<CarDTO> resultList = null;
        if(cars != null){
            resultList = new ArrayList<CarDTO>();
            for(Car car: cars){
                resultList.add(assembleCarDTO(car));
            }
        }
        return resultList;
    }

    private static CarDTO assembleCarDTO(Car car){
        CarDTO dto = new CarDTO();
        dto.setCapacity(car.getCapacity());
        dto.setDescription(car.getDescription());
        dto.setModel(car.getModel());
        dto.setType(car.getType());
        dto.setYear(car.getYear());
        dto.setVendor(car.getVendor());
        dto.setId(car.getId());
        if(car.getUser()!=null){
            dto.setUser(assembleUserDTO(car.getUser()));
        }
        return dto;
    }

    private static UserDTO assembleUserDTO(User user){
        UserDTO dto = new UserDTO();
        dto.setId(user.getId());
        dto.setPhone(user.getPhone());
        dto.setEmail(user.getEmail());
        dto.setName(user.getName());
        return dto;
    }


}
