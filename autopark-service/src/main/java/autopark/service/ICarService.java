package autopark.service;

import autopark.dto.CarDTO;

import java.util.List;

/**
 * Created by  01 on 15.02.2016.
 */
public interface ICarService {
    List<CarDTO> getCars();
}
